# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Full proof of concept, front-end, backend, application, algorithm and CI source for strike a chord app
* Also includes all test audio data
* Demo examples

### How do I get set up? ###

* For POC jupyter notebook: 
	$pip install notebook
	$ jupyter notebook &
	Open src/notebookTrials/ChordExtractionExperiment.ipynb

* Dependencies:
	- python libraries : ### FIXME ###
	- NPM / Node.js
	- pip install pipenv
	- npm install -g @angular/cli
	- Docker required for backend
	
* Database configuration

* How to run tests:
	- Try example tests: 
* Deployment instructions

### Contribution guidelines ###

* Writing tests:
	- Required for algorithm: Test audio & JSON containing chords and timestamps
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner: Daire Canavan - dairecan11@gmail.com