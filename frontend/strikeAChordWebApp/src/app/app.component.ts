import { Component } from '@angular/core';
import * as RecordRTC from 'recordrtc';
import { DomSanitizer } from '@angular/platform-browser';
import { StereoAudioRecorder } from 'recordrtc';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'strikeAChordWebApp';

  //Lets declare Record OBJ
  //Lets declare Record OBJ
  record;
  recorded = false;
//Lets declare Record OBJ
//URL of Blob
url: string | undefined;
  error!: string;
//Will use this flag for toggeling recording
recording = false;
constructor(private domSanitizer: DomSanitizer) {}
sanitize(url: string) {
return this.domSanitizer.bypassSecurityTrustUrl(url);
}
/**
* Start recording.
*/
initiateRecording() {
this.recording = true;
let mediaConstraints = {
video: false,
audio: true
};
//alert('recording');
navigator.mediaDevices.getUserMedia(mediaConstraints).then(this.successCallback.bind(this), this.errorCallback.bind(this));
}
/**
* Will be called automatically.
*/
successCallback(stream: any) {
var options = {
mimeType: "audio/wav",
numberOfAudioChannels: 1,
sampleRate: 48000,
};
//Start Actuall Recording
if(!this.recorded){
  var StereoAudioRecorder = RecordRTC.StereoAudioRecorder;
  this.record = new StereoAudioRecorder(stream, options);
  this.record.record();
}
else{
  this.record.record();
}

}
/**
* Stop recording.
*/
stopRecording() {
this.recording = false;
//alert(' not recording');
this.recorded = true;
this.record.stop(this.processRecording.bind(this));
}
/**
* processRecording Do what ever you want with blob
* @param  {any} blob Blog
*/
processRecording(blob: any) {
this.url = URL.createObjectURL(blob);
console.log("blob", blob);
console.log("url", this.url);
alert(this.url)
}
/**
* Process Error.
*/
errorCallback(error: any) {
this.error = 'Can not play audio in your browser';
}
ngOnInit() {}
}